const express = require('express');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();
const User = require("../models/user");
const passportJWT = require("../middlewares/passport-jwt");
const connectRabbitMQ = require("../config/rabbitmq");

//get user's profile
/* localhost:4000/api/v1/users/profile */
router.get('/profile', [passportJWT.isLogin] , async function(req, res, next) {
  const user = await User.findByPk(req.user.user_id);
  return res.status(200).json({
    id: user.id,
    fullname: user.fullname,
    email: user.email,
    role: user.role,
  });
});

/* localhost:4000/api/v1/users/ */
router.get('/', function(req, res, next) {
  res.send('Hello Users');
});

/* localhost:4000/api/v1/users/register */
router.post("/register", async function (req, res, next) {
  const { fullname, email, password } = req.body;

  //check email ซ้ำ
  const user = await User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(400).json({ message: "มีผู้ใช้งานนี้ในระบบแล้ว" });
  }

  //hash รหัสผ่าน
  const mySalt = await bcrypt.genSalt();
  const passwordHash = await bcrypt.hash(password, mySalt);

  //สร้าง User ใหม่
  const newUser = await User.create({
    fullname: fullname,
    email: email,
    password: passwordHash,
  });

  //1. connect to rabbitmq server
  const channel = await connectRabbitMQ();

  // //2. ระบุ Exchange และส่งแบบ direct
  // await channel.assertExchange("ex.akenarin.direct", "direct", {
  //   durable: true,
  // });

  // //3. ระบุ Queue และส่งไปที่ product-service
  // await channel.assertQueue("q.akenarin.direct.product.service", {
  //   durable: true,
  // });

  //4. ส่งข้อความ (pubish message) ไปยัง product-service
  await channel.publish(
    "ex.akenarin.direct",
    "rk.akenarin.product.service",
    Buffer.from(
      JSON.stringify({
        id: newUser.id,
        fullname: newUser.fullname,
        role: newUser.role,
      })
    ),
    {
      contentType: "application/json",
      contentEncoding: "utf-8",
      type: "UserCreated",
      persistent: true,
    }
  );

  //ส่งแบบ fanout (อย่าลืม connect rabbitmq ตามข้อ 1. ก่อน)
  //ส่งข้อความ (pubish message) ไปยัง service ต่างๆ
  await channel.publish(
    "ex.akenarin.fanout",
    "",
    Buffer.from(
      JSON.stringify({
        message: "ข้อความนี้มาจาก auth-service ส่งแบบ fanout again"
      })
    ),
    {
      contentType: "application/json",
      contentEncoding: "utf-8",
      type: "UserFanout",
      persistent: true,
    }
  );

  return res.status(201).json({
    message: "ลงทะเบียนสำเร็จ",
  });
});

/* localhost:4000/api/v1/users/login */
router.post("/login", async function (req, res, next) {
  const {email, password} = req.body;

  //1.นำ email มาตรวจสอบในระบบว่ามีผู้ใช้นี้หรือไม่
  const user = await User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({ message: "ไม่พบผู้ใช้นี้ในระบบ" });
  }

  //2.ถ้ามี ให้นำรหัสผ่านจาก body มาเปรียบเทียบกับรหัสผ่านจากผู้ใช้ในข้อ 1
  const isValid = await bcrypt.compare(password, user.password);
  if (isValid === false) {
    return res.status(401).json({ message: "รหัสผ่านไม่ถูกต้อง" });
  } 

  //3.สร้าง token ส่งให้ client/service ที่เป็น UI
  const token = jwt.sign({
    user_id: user.id,
    user_role: user.role
  }, process.env.JWT_KEY, {
    expiresIn: "7d",
  });

  return res.status(200).json({
    message: "เข้าระบบสำเร็จ",
    access_token: token
  });
});

module.exports = router;
